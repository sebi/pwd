<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Data;

use App\Entity\Portes;
use App\Entity\Categorie;
use App\Entity\Marque;
use App\Entity\Modele;
/**
 * Description of SearchData
 *
 * @author Seb
 */
class SearchData {
    //put your code here
    /**
     *
     * @var string
     */
    public $q ='';
    /**
     *
     * @var Categorie
     */
    public $categorie;
    /**
     *
     * @var Modele
     */
    public $modele;
    /**
     *
     * @var Portes
     */
    public $portes;
    /**
     *
     * @var Marque
     */
    public $marque;
}
