<?php
/**
 * Created by IntelliJ IDEA.
 * User: sebastien
 * Date: 16/01/20
 * Time: 11:15
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Marque;
class MarqueFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $marque = new Product();
        // $manager->persist($marque);
        // create 20 Users! Bam!
        for ($i = 0; $i < 20; $i++) {
            $marque = new Marque();
            $marque->setNom('Ferrari'.$i);
            $manager->persist($marque);
        }

        $manager->flush();
    }
}