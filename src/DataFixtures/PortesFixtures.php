<?php
/**
 * Created by IntelliJ IDEA.
 * User: sebastien
 * Date: 16/01/20
 * Time: 10:59
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Portes;

class PortesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $portes = new Product();
        // $manager->persist($portes);
        // create 20 Users! Bam!
        for ($i = 2; $i < 7; $i++) {
            $portes = new Portes();
            $portes->setNombreDePortes($i);
            $manager->persist($portes);
        }

        $manager->flush();
    }
}