<?php
/**
 * Created by IntelliJ IDEA.
 * User: sebastien
 * Date: 16/01/20
 * Time: 11:12
 */
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Categorie;
class CategorieFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        // $categorie = new Product();
        // $manager->persist($categorie);
        // create 20 Users! Bam!
        for ($i = 0; $i < 20; $i++) {
            $categorie = new Categorie();
            $categorie->setNom('SUV'.$i);
            $manager->persist($categorie);
        }

        $manager->flush();
    }
}