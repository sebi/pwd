<?php
/**
 * Created by IntelliJ IDEA.
 * User: sebastien
 * Date: 16/01/20
 * Time: 12:04
 */

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Modele;
class ModeleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $modele = new Product();
        // $manager->persist($modele);
        // create 20 Users! Bam!
        for ($i = 0; $i < 20; $i++) {
            $modele = new Modele();
            $modele->setNom('Aventador'.$i);
            $manager->persist($modele);
        }

        $manager->flush();
    }
}