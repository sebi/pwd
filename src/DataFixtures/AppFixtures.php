<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $user = new Product();
        // $manager->persist($user);
        // create 20 Users! Bam!
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setPseudo('pseudo '.$i);
            $user->setNom('Marci'.$i);
            $user->setPrenom('Seb'.$i);
            $user->setEmail('Seb'.$i.'@hotmail.com');
            $user->setPhoto('test.jpg');
            $manager->persist($user);
        }

        $manager->flush();
    }
}
