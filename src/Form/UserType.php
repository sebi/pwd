<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null,[
                'attr'=> ['placeholder' => 'Encodez votre pseudo']
                ])
            ->add('nom', null,[
                'attr'=> ['placeholder' => 'Encodez votre nom']
                ])
            ->add('prenom', null,[
                'attr'=> ['placeholder' => 'Encodez votre prenom']
                ])
            ->add('email', null,[
                'attr'=> ['placeholder' => 'Encodez votre email']
                ])
            ->add('password', PasswordType::class,[
                'attr'=> ['placeholder' => 'Encodez votre mot de passe']
                    ])
            ->add('photo', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize' => '100k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid jpeg or png',
                    ])
                ],
            ])
            
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
