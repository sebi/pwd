<?php

namespace App\Form;

use App\Data\SearchData;
use App\Entity\Categorie;
use App\Entity\Marque;
use App\Entity\Modele;
use App\Entity\Portes;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder,array $options)
    {
        $builder
            ->add('categorie', EntityType::class,[
                'class'=>Categorie::class,

                'label'=>false,
                'required'=>false,
                'placeholder' => 'Categorie'
                    ])
            ->add('marque', EntityType::class,[
                'class'=>Marque::class,

                'label'=>false,
                'required'=>false,
                'placeholder' => 'Marque'
                    ])
            ->add('modele', EntityType::class,[
                'class'=>Modele::class,

                'label'=>false,
                'required'=>false,
                'placeholder' => 'Modele'
                    ])
            ->add('portes', EntityType::class,[
                'class'=>Portes::class,

                'label'=>false,
                'required'=>false,
                'placeholder' => 'Portes'
                    ])
            ->add('q', TextType::class,[
                'label'=>false,
                'required'=>false,
                'attr'=>[
                    'placeholder'=>'Recherche'
                ]
               
                    ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class'=>SearchData::class,
            'method'=>'GET',
            'crs_protection' => false
        ]);
    }
    public function getBlockPrefix() {
        return '';
        
    }
}
