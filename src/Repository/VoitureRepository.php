<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Voiture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Voiture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Voiture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Voiture[]    findAll()
 * @method Voiture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoitureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Voiture::class);
    }

    // /**
    //  * @return Voiture[] Returns an array of Voiture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Voiture
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findSearch(SearchData $search){
        $query = $this
                ->createQueryBuilder('v');
                
                if(!empty($search->categorie)){
                    $query = $query
                            ->select('v')
                            //->from('Voiture','v')
                            ->andWhere('v.categorie = :categorie')
                            ->setParameter('categorie',"{$search->categorie->getId()}");
                }
                if(!empty($search->modele)){
                    $query = $query
                            ->select('v')
                            //->from('Voiture','v')
                            ->andWhere('v.modele = :modele')
                            ->setParameter('modele',"{$search->modele->getId()}");

                }
                if(!empty($search->portes)){
                    $query = $query
                        ->select('v')
                        //->from('Voiture','v')
                        ->andWhere('v.portes = :portes')
                        ->setParameter('portes',"{$search->portes->getId()}");

                }
                if(!empty($search->marque)){
                    $query = $query
                        ->select('v')
                        //->from('Voiture','v')
                        ->andWhere('v.marque = :marque')
                        ->setParameter('marque',"{$search->marque->getId()}");

                }
                if(!empty($search->q)){
                    $query = $query
                        ->select('v')
                        //->from('Voiture','v')
                        ->andWhere('v.nom LIKE :q')
                        ->setParameter('q',"%{$search->q}%");
                    dump($query);
                }
                //->join('v.categorie','c')
        
        return $query->getQuery()->getResult();
    }
    public function findNewCars(){
        $query = $this
                ->createQueryBuilder('v')
                ->select('v')
                ->orderBy('v.id', 'DESC')
                ->setMaxResults(3);
       return $query->getQuery()->getResult();
    }
    
}
