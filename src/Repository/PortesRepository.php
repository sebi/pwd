<?php

namespace App\Repository;

use App\Entity\Portes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Portes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Portes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Portes[]    findAll()
 * @method Portes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PortesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Portes::class);
    }

    // /**
    //  * @return Portes[] Returns an array of Portes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Portes
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
