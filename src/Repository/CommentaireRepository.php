<?php

namespace App\Repository;

use App\Entity\Commentaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

    // /**
    //  * @return Commentaire[] Returns an array of Commentaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commentaire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /*
    public function findAllByIdCar(int $id){
        $query = $this
                ->createQueryBuilder('c');
                
             
                    $query = $query
                            ->select('c')
                            ->andWhere('c.voiture = '.$id.'');
                           
                
        
        return $query->getQuery()->getResult();
    }
     * */
    public function findAllByIdCar(int $id){
        /*
        return $this->createQueryBuilder('c')
                     ->select('c')
                     ->andWhere('c.voiture = '.$id.'')
                     ->getQuery()
                     ->getResult();
         * */
        
       
        $query = $this
                ->createQueryBuilder('c');
                
             
                    $query = $query
                            ->select('c')
                            ->andWhere('c.voiture = '.$id.'');
                           
                
        
        return $query->getQuery()->getResult();
        
         
    }
     
    public function findAllByIdUser(int $id){
        $query = $this
            ->createQueryBuilder('c');


        $query = $query
            ->select('c')
            ->andWhere('c.user = '.$id.'');



        return $query->getQuery()->getResult();
    }
    public function findNewComment(){
        $query = $this
                ->createQueryBuilder('c')
                ->select('c')
                ->orderBy('c.date', 'DESC')
                ->setMaxResults(3);
       return $query->getQuery()->getResult();
    }
    public function findAllOrderByReport(){
        $query = $this
                ->createQueryBuilder('c')
                ->select('c')
                ->orderBy('c.signalement', 'DESC');
       return $query->getQuery()->getResult();
    }
}
