<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Commentaire;
use App\Form\CommentType;
use App\Form\SearchType;
use App\Repository\VoitureRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function dump;

/**
 * Description of PostController
 *
 * @author Seb
 */
class PostController extends AbstractController {
    //put your code here
        /**
     * @Route("/post",name="post")
     */
    public function post(Request $request, EntityManagerInterface $entityManager, VoitureRepository $vreprository) {
        $dataBar = new SearchData();
        $comment = new Commentaire();
        $user = $this->getUser();
        $id = $request->query->get('id');
        $voiture = $vreprository->find($id);
        
        dump($voiture);
        
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
       

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        
        $post = $this->createForm(CommentType::class,$comment);
        
        $post->handleRequest($request);
        if ($post->isSubmitted() && $post->isValid()) {
            
            $comment->setDate(new DateTime());
            $comment->setUser($user);
            //$comment->setCote(3);
            $comment->setVoiture($voiture);
            $entityManager->persist($comment);
            $entityManager->flush();
            return $this->redirectToRoute('home');
            
        }
        
        

        return $this->render('pages/post.html.twig',array('searchBar' => $searchBar->createView(),'comment' => $post->createView()));
    }
}
