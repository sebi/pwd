<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Form\ContactType;
use App\Form\SearchType;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function dump;





class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, Swift_Mailer $mailer) {
        $form = $this->createForm(ContactType::class);
        
        $dataBar = new SearchData();
        
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contactForm = $form->getData();

            $message = (new Swift_Message('Email'))
                ->setFrom('demo@bes-webdeveloper-seraing.be')
                ->setTo('sebastien.marcipont.ipeps@gmail.com')
                ->setSubject('Nouveau message reçu de l\'utilisateur [' . $contactForm['name'] . ']')
                ->setBody('Message reçu de [' . $contactForm['name'] . '] : ' . $contactForm['message'],'text/plain')
            ;

            $mailer->send($message);
            

            $this->addFlash(
                'success',
                'Votre message a été envoyé'
            );
 
 

            return $this->redirectToRoute('contact');
        }

        return $this->render('pages/contact.html.twig', [
            'searchBar' => $searchBar->createView(),
            'contactform' => $form->createView()
            
        ]);
    }
}
