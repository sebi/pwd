<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Categorie;
use App\Entity\Marque;
use App\Entity\Modele;
use App\Entity\Voiture;
use App\Form\CarEditType;
use App\Form\CarType;
use App\Form\CategorieType;
use App\Form\MarqueType;
use App\Form\ModeleType;
use App\Form\UserEditType;
use App\Repository\CategorieRepository;
use App\Repository\CommentaireRepository;
use App\Repository\MarqueRepository;
use App\Repository\ModeleRepository;
use App\Repository\UserRepository;
use App\Repository\VoitureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="adminhome")
     */
    public function index()
    {
        
        return $this->render('admin/adminhome.html.twig');
    }
    /**
     * @Route("/admin/utilisateurs", name="utilisateurs")
     */
    public function usersList(Request $request,UserRepository $users)
    {
        $dataBar = new SearchData();

       
        
        return $this->render('admin/users.html.twig', [
            'users' => $users->findAll()
        ]);
    }
    /**
     * @Route("/admin/utilisateurs/edit", name="utilisateuredit")
     */
    public function userEdit(Request $request,UserRepository $usersrepo,EntityManagerInterface $em)
    {
        $id = $request->query->get('id');
        $user = $usersrepo->find($id);
        $form = $this->createForm(UserEditType::class,$user);

        $form->handleRequest($request);
         if($form->isSubmitted() && $form->isValid()){
             $em->persist($user);
             $em->flush();
             
             //$this->addFlash('message','utilisateur moddifier avec succès');
             return $this->redirectToRoute('utilisateurs');

         }
        
        return $this->render('admin/useredit.html.twig', [
            'user' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin/utilisateurs/delete", name="userdelete")
     */
    public function userDelete(Request $request,UserRepository $usersrepo,EntityManagerInterface $em)
    {
       
        $id = $request->query->get('id');
        $userDelete = $usersrepo->find($id);
        $em->remove($userDelete);
        //$em->persist($voitureDelete);
        $em->flush();
        return $this->redirectToRoute('utilisateurs');

        
       
    }
    /**
     * @Route("/admin/voitures", name="voitures")
     */
    public function carsList(Request $request,VoitureRepository $voitures)
    {
        $dataBar = new SearchData();

       
        
        return $this->render('admin/cars.html.twig', [
            'voitures' => $voitures->findAll()
        ]);
    }
    /**
     * @Route("/admin/voitures/edit", name="editcars")
     */
    public function carsEdit(Request $request,VoitureRepository $vreprository,EntityManagerInterface $em)
    {
       
       // $voiture = new Voiture();
        $id = $request->query->get('id');
        $voitureEdit = $vreprository->find($id);
        
        $form = $this->createForm(CarEditType::class,$voitureEdit);
        
        
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($voitureEdit);
            $em->flush();
            return $this->redirectToRoute('voitures');
            
        }
        
        

       
        
        return $this->render('admin/voitureedit.html.twig', [
            
            'form' =>$form->createView()
        ]);
    }
    /**
     * @Route("/admin/voitures/add", name="addcars")
     */
    public function carsAdd(Request $request,EntityManagerInterface $em)
    {
      
        $voiture = new Voiture();
    
     
        
        $form = $this->createForm(CarType::class,$voiture);
        
        
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $photoFile = $form['photo']->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($photoFile) {
                $originalFilename = $photoFile->getClientOriginalName();
                // this is needed to safely include the file name as part of the URL
                //$safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
               // $newFilename = $safeFilename.'-'.uniqid().'.'.$photoFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $photoFile->move(
                        $this->getParameter('photo_directory'),
                        $originalFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $voiture->setPhoto($originalFilename);
            }
            
            
            $em->persist($voiture);
            $em->flush();
            return $this->redirectToRoute('voitures');
            
        }
        
        

       
        
        return $this->render('admin/voitureadd.html.twig', [
            
            'form' =>$form->createView()
        ]);
    }
    /**
     * @Route("/admin/voitures/delete", name="deletecars")
     */
    public function carsDelete(Request $request,VoitureRepository $vreprository,EntityManagerInterface $em)
    {
       
        $id = $request->query->get('id');
        $voitureDelete = $vreprository->find($id);
        $em->remove($voitureDelete);
        //$em->persist($voitureDelete);
        $em->flush();
        return $this->redirectToRoute('voitures');

        
       
    }
        /**
     * @Route("/admin/modeles", name="modeles")
     */
    public function modeleList(Request $request,ModeleRepository $morepo)
    {
      
        return $this->render('admin/modeles.html.twig', [
            'modeles' => $morepo->findAll()
        ]);
    }
        /**
     * @Route("/admin/modeles/add", name="modeleadd")
     */
    public function modeleAdd(Request $request,ModeleRepository $morepo,EntityManagerInterface $em)
    {
        $modele = new Modele();
        
        $form = $this->createForm(ModeleType::class,$modele);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            $em->persist($modele);
            $em->flush();
            return $this->redirectToRoute('modeles');
            
        }
        
      
        return $this->render('admin/modeleadd.html.twig', [
            'form' =>$form->createView()
        ]);
    }
         /**
     * @Route("/admin/marques", name="marques")
     */
    public function marqueList(Request $request,MarqueRepository $marepo)
    {
      
        return $this->render('admin/marques.html.twig', [
            'marques' => $marepo->findAll()
        ]);
    }
        /**
     * @Route("/admin/marques/add", name="marqueadd")
     */
    public function marqueAdd(Request $request,MarqueRepository $marepo,EntityManagerInterface $em)
    {
        $marque = new Marque();
        
        $form = $this->createForm(MarqueType::class,$marque);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            $em->persist($marque);
            $em->flush();
            return $this->redirectToRoute('marques');
            
        }
        
      
        return $this->render('admin/marqueadd.html.twig', [
            'form' =>$form->createView()
        ]);
    }
             /**
     * @Route("/admin/categories", name="categories")
     */
    public function categorieList(Request $request,CategorieRepository $carepo)
    {
      
        return $this->render('admin/categories.html.twig', [
            'categories' => $carepo->findAll()
        ]);
    }
        /**
     * @Route("/admin/categories/add", name="categorieadd")
     */
    public function categorieAdd(Request $request,CategorieRepository $carepo,EntityManagerInterface $em)
    {
        $categorie = new Categorie();
        
        $form = $this->createForm(CategorieType::class,$categorie);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            $em->persist($categorie);
            $em->flush();
            return $this->redirectToRoute('categories');
            
        }
        
      
        return $this->render('admin/categorieadd.html.twig', [
            'form' =>$form->createView()
        ]);
    }
     /**
     * @Route("/admin/commentaires", name="commentaires")
     */
    public function commentaireList(Request $request,CommentaireRepository $comrepo)
    {
      
        return $this->render('admin/commentaires.html.twig', [
            'commentaires' => $comrepo->findAllOrderByReport()
        ]);
    }
     /**
     * @Route("/admin/commentaires/delete", name="deletecommentaire")
     */
    public function commentaireDelete(Request $request,CommentaireRepository $comrepo,EntityManagerInterface $em)
    {
       
        $id = $request->query->get('id');
        $comDelete = $comrepo->find($id);
        $em->remove($comDelete);
        //$em->persist($voitureDelete);
        $em->flush();
        return $this->redirectToRoute('commentaires');

        
       
    }
}
