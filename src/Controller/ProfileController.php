<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Form\SearchType;
use App\Form\UserEditType;
use App\Repository\CommentaireRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function dump;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index()
    {
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }
     /**
     * @Route("/myprofil", name="myprofil")
     */
   
     public function myProfil(UserRepository $userrepo)
    {
        $dataBar = new SearchData();
        
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        
        $users = $userrepo->findAll();
        return $this->render('pages/myprofile.html.twig', [
            'searchBar' => $searchBar->createView(),
            'users' => $users
        ]);
    }
    /**
     * @Route("/myprofil/edit", name="myprofiledit")
     */
   
     public function myProfilEdit(Request $request,EntityManagerInterface $em,UserRepository $userrepo)
    {
        $dataBar = new SearchData();
        
        $id = $request->query->get('id');
        $userEdit = $userrepo->find($id);
        
        $form = $this->createForm(UserEditType::class,$userEdit);
        $form->remove('roles');
        
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($userEdit);
            $em->flush();
            return $this->redirectToRoute('myprofil');
            
        }
        
        return $this->render('pages/myprofiledit.html.twig', [
            'searchBar' => $searchBar->createView(),
            'form' => $form->createView()
        ]);
    }

        /**
     * @Route("/myprofil/deletecom", name="myprofildeletecom")
     */
   
     public function myProfilDeleteCom(Request $request,EntityManagerInterface $em,UserRepository $userrepo,CommentaireRepository $comrepo )
    {
        $dataBar = new SearchData();
        
        //$user = $this->getUser();

        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');

        $id = $request->query->get('id');
        $user = $userrepo->find($id);
        $commentaires = $comrepo->findAllByIdUser($id);
        dump($commentaires);

        return $this->render('pages/myprofildeletecom.html.twig',array('searchBar' => $searchBar->createView(),'user'=>$user,'commentaires'=>$commentaires));
    }
      /**
     * @Route("/myprofil/deletecom/delete", name="deletemycommentaire")
     */
    public function mycommentaireDelete(Request $request,CommentaireRepository $comrepo,EntityManagerInterface $em)
    {
       
        $id = $request->query->get('id');
        $comDelete = $comrepo->find($id);
        $em->remove($comDelete);
        //$em->persist($voitureDelete);
        $em->flush();
        return $this->redirectToRoute('myprofil');

        
       
    }
  
    
}
