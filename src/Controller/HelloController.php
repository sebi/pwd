<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\User;
use App\Form\LoginType;
use App\Form\SearchType;
use App\Repository\CommentaireRepository;
use App\Repository\UserRepository;
use App\Repository\VoitureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function dump;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HelloController
 *
 * @author Seb
 */
class HelloController extends AbstractController{
    //put your code here
    /**
     * @Route("/test",name="test")
     */
    public function test(){
        
        return new Response('Test </body>');
    }

    /**
     * @Route("/search",name="search")
     */
    public function search(Request $request, VoitureRepository $vreprository){
        
        $data = new SearchData();
        $dataBar = new SearchData();

        
        
        $form = $this->createForm(SearchType::class,$data);
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        $form->handleRequest($request);


        $voitures = $vreprository->findSearch($data);
       /*$voitures = $paginator->paginate(
               $this->$vreprository->findSearch($data),
               $request->query()->getInt('page',1),
               5
               );
        * */
        
               

       //$form = $this->createForm(SearchType::class);
 
       
      
      
    
       

        return $this->render('pages/search.html.twig', ['voitures'=>$voitures,'form' => $form->createView(),'searchBar' => $searchBar->createView()]);
    }
    /**
     * @Route("/",name="home")
     */
    public function home(Request $request, EntityManagerInterface $entityManager,VoitureRepository $vrepo,CommentaireRepository $comrep){
/*
        $categorie = $catrepository->findAll();
        dump($categorie);
*/
        $data = new SearchData();
        $dataBar = new SearchData();
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        $form = $this->createForm(SearchType::class,$data, ['action' => $this->generateUrl('search')]);
        
        $listvoiture = $vrepo->findNewCars();
        $listcommentaire = $comrep->findNewComment();
        
        //dump($listcommentaire);

        
        //$form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            return $this->redirectToRoute('search');
            
        }
        if ($searchBar->isSubmitted() && $form->isValid()) {


            return $this->redirectToRoute('search');

        }

        return $this->render('pages/home.html.twig', array('form' => $form->createView(),'searchBar' => $searchBar->createView(),'voitures' =>$listvoiture,'commentaires' =>$listcommentaire));
    }
    /**
     * @Route("/connexion",name="connexion")
     */
    public function connexion(){
        $dataBar = new SearchData();
        $user = new User();
        $login = -$this->createForm(LoginType::class, $user);


        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');


        return $this->render('pages/connexion.html.twig',array('searchBar' => $searchBar->createView()));
    }
    /**
     * @Route("/carPage",name="carPage")
     */
    public function carPage(Request $request, EntityManagerInterface $entityManager,VoitureRepository $vreprository, CommentaireRepository $comrepo, UserRepository $userepo){
        
        $dataBar = new SearchData();
        
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);

        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        
        $id = $request->query->get('id');
        
        $voiture = $vreprository->find($id);
        
        $commentaires = $comrepo->findAllByIdCar($id);
        /*
        $commmentaires= $paginator->paginate(
                $commmentaires=$comrepo->findAllByIdCar($id),
                $request->query->getInt('page',1),
                2
                );
         * */
         
        
       //dump($voiture);
       //dump($commentaires);
        
      
        
        //$comrepo->foundCarById();

        return $this->render('pages/carPage.html.twig',array('searchBar' => $searchBar->createView(),'voiture'=>$voiture,'commentaires'=>$commentaires));
    }

    /**
     * @Route("/profileuser",name="profileuser")
     */
    public function profileuser(Request $request, UserRepository $userepo){

        $dataBar = new SearchData();

        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');

        $id = $request->query->get('id');
        $user = $userepo->find($id);
        dump($user);

        return $this->render('pages/profileuser.html.twig',array('searchBar' => $searchBar->createView(),'user'=>$user));
    }
    /**
     * @Route("/userCommentaires",name="userCommentaires")
     */
    public function userCommentaires(Request $request, UserRepository $userepo,CommentaireRepository $comrepo){

        $dataBar = new SearchData();

        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');

        $id = $request->query->get('id');
        $user = $userepo->find($id);
        $commentaires = $comrepo->findAllByIdUser($id);
        dump($commentaires);

        return $this->render('pages/userCommentaires.html.twig',array('searchBar' => $searchBar->createView(),'user'=>$user,'commentaires'=>$commentaires));
    }
     


    

}
