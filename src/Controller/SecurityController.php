<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\User;
use App\Form\SearchType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use function dump;

class SecurityController extends AbstractController
{
    
        /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder) {
        // 1) build the form
        $user = new User();
        $dataBar = new SearchData();
        
        $form = $this->createForm(UserType::class, $user);
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
  /** @var UploadedFile $photoFile */
            $photoFile = $form['photo']->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($photoFile) {
                $originalFilename = $photoFile->getClientOriginalName();
                // this is needed to safely include the file name as part of the URL
                //$safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
               // $newFilename = $safeFilename.'-'.uniqid().'.'.$photoFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $photoFile->move(
                        $this->getParameter('photo_directory'),
                        $originalFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $user->setPhoto($originalFilename);
            }
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            
            $entityManager->persist($user);
            $entityManager->flush();
            
            dump($user);
            

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('security_login');
        }

        return $this->render(
                        'security/inscription.html.twig', array('form' => $form->createView(),'searchBar' => $searchBar->createView())
        );
    }
      /**
     * @Route("/login", name="security_login")
     */
    public function login(){
        $dataBar = new SearchData();
        
       
        $searchBar = $this->createForm(SearchType::class,$dataBar, ['action' => $this->generateUrl('search')]);
        $searchBar->remove('modele');
        $searchBar->remove('portes');
        $searchBar->remove('categorie');
        $searchBar->remove('marque');
        
        return $this->render('security/login.html.twig', array('searchBar' => $searchBar->createView()));
        
    }
    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){
        
        
    }
}
