<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortesRepository")
 */
class Portes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $nombre_de_portes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voiture", mappedBy="portes")
     */
    private $voitures;

    public function __construct()
    {
        $this->voitures = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombreDePortes()
    {
        return $this->nombre_de_portes;
    }

    public function setNombreDePortes(string $nombre_de_portes): self
    {
        $this->nombre_de_portes = $nombre_de_portes;

        return $this;
    }

    /**
     * @return Collection|Voiture[]
     */
    public function getVoitures(): Collection
    {
        return $this->voitures;
    }

    public function addVoiture(Voiture $voiture): self
    {
        if (!$this->voitures->contains($voiture)) {
            $this->voitures[] = $voiture;
            $voiture->setPortes($this);
        }

        return $this;
    }

    public function removeVoiture(Voiture $voiture): self
    {
        if ($this->voitures->contains($voiture)) {
            $this->voitures->removeElement($voiture);
            // set the owning side to null (unless already changed)
            if ($voiture->getPortes() === $this) {
                $voiture->setPortes(null);
            }
        }

        return $this;
    }
   public function __toString(){
        return (string)$this -> nombre_de_portes;
    }
}
