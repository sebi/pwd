<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $cote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pointPositif;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pointNegatif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $signalement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Voiture", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voiture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commentaire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;
    public function __construct()
    {
        $this->date = date("Y-m-d H:i:s");
        $this->signalement = 0;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getCote()
    {
        return $this->cote;
    }

    public function setCote(int $cote): self
    {
        $this->cote = $cote;

        return $this;
    }

    public function getPointPositif()
    {
        return $this->pointPositif;
    }

    public function setPointPositif(string $pointPositif): self
    {
        $this->pointPositif = $pointPositif;

        return $this;
    }

    public function getPointNegatif()
    {
        return $this->pointNegatif;
    }

    public function setPointNegatif(string $pointNegatif): self
    {
        $this->pointNegatif = $pointNegatif;

        return $this;
    }

    public function getSignalement()
    {
        return $this->signalement;
    }

    public function setSignalement(int $signalement)
    {
        $this->signalement = $signalement;

        return $this;
    }

    public function getVoiture()
    {
        return $this->voiture;
    }

    public function setVoiture(Voiture $voiture): self
    {
        $this->voiture = $voiture;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
